import {Component, OnInit} from '@angular/core';
import {GotApiService} from "../webservices/got-api.service";
import {House} from "../webservices/got-api.model";

@Component({
  selector: 'app-houses-list',
  templateUrl: './houses-list.component.html',
  styleUrls: ['./houses-list.component.scss']
})
export class HousesListComponent implements OnInit {
  houses: Map<string, House[]> = new Map();

  constructor(private gotApiService: GotApiService) { }

  ngOnInit(): void {
    this.gotApiService.getHouses().subscribe(houses => {
      houses.forEach(house => {
        house.id = house.url.substring(house.url.lastIndexOf('/') + 1);
        if (!this.houses.has(house.region)) this.houses.set(house.region, []);
        // @ts-ignore
        this.houses.get(house.region).push(house);
      })
    });
  }

}
