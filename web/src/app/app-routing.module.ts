import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HousesListComponent} from "./houses-list/houses-list.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/houses',
    pathMatch: 'full'
  },
  {
    path: 'houses',
    component: HousesListComponent
  },
  {
    path: 'houses/:id',
    loadChildren: () => import('./houses-detail/houses-detail.module').then(m => m.HousesDetailModule)
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
