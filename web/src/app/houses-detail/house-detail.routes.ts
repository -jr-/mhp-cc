import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {HousesDetailComponent} from "./houses-detail.component";

const routes: Routes = [
  {
    path: '',
    component: HousesDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseDetailRoutes {}
