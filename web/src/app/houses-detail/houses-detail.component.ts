import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {GotApiService} from "../webservices/got-api.service";
import {Character, House} from "../webservices/got-api.model";

@Component({
  selector: 'app-houses-detail',
  templateUrl: './houses-detail.component.html',
  styleUrls: ['./houses-detail.component.scss']
})
export class HousesDetailComponent implements OnInit {

  house: House | null = null;
  characters: Map<string, Character> = new Map();
  relatedHouses: Map<string, House> = new Map();

  constructor(private route: ActivatedRoute, private gotApiService: GotApiService) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id !== null) {
      this.gotApiService.getHouse(id).subscribe(house => {
        house.id = house.url.substring(house.url.lastIndexOf('/') + 1);
        this.extractCharacters(house);
        this.house = house;
      });
    }
  }

  private extractCharacters(house: House): void {
    this.fetchAndAddHouses(house.overlord);
    house.cadetBranches.forEach(cadetUrl => this.fetchAndAddHouses(cadetUrl));

    this.fetchAndAddCharacter(house.currentLord);
    this.fetchAndAddCharacter(house.overlord);
    this.fetchAndAddCharacter(house.founder);
    this.fetchAndAddCharacter(house.heir);
    house.swornMembers.forEach(memberUrl => this.fetchAndAddCharacter(memberUrl));
  }

  private fetchAndAddHouses(housesUrl: string): void {
    if (!housesUrl) return;
    if (!this.relatedHouses.has(housesUrl)) {
      const id = housesUrl.substring(housesUrl.lastIndexOf('/') + 1);
      this.gotApiService.getHouse(id).subscribe(house => this.relatedHouses.set(house.url, house));
    }
  }

  private fetchAndAddCharacter(characterUrl: string): void {
    if (!characterUrl) return;
    if (!this.characters.has(characterUrl)) {
      const id = characterUrl.substring(characterUrl.lastIndexOf('/') + 1);
      this.gotApiService.getCharacter(id).subscribe(character => this.characters.set(character.url, character));
    }
  }
}
