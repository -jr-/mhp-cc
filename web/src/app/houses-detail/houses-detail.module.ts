import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HousesDetailComponent } from './houses-detail.component';
import {HouseDetailRoutes} from "./house-detail.routes";

@NgModule({
  declarations: [
    HousesDetailComponent
  ],
  imports: [
    CommonModule,
    HouseDetailRoutes
  ]
})
export class HousesDetailModule { }
