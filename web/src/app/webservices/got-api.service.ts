import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";
import {Character, House} from "./got-api.model";

@Injectable({ providedIn: 'root' })
export class GotApiService {
  private apiRoot = 'https://www.anapioficeandfire.com/api/';

  constructor(private http: HttpClient) { }

  getHouses(): Observable<House[]> {
    const path = this.apiRoot + 'houses';
    return this.http.get<House[]>(path);
  }

  getHouse(id: string): Observable<House> {
    const path = `${this.apiRoot}houses/${id}`;
    return this.http.get<House>(path);
  }

  getCharacter(id: string): Observable<Character> {
    const path = `${this.apiRoot}characters/${id}`;
    return this.http.get<Character>(path);
  }

  parseIdFromUrl(url: string) {
    return url.substring(url.lastIndexOf('/') + 1);
  }
}
