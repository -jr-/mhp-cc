package com.mhp.coding.challenges.mapping.mappers

import com.mhp.coding.challenges.mapping.exceptions.MappingException
import com.mhp.coding.challenges.mapping.models.db.Article
import com.mhp.coding.challenges.mapping.models.db.Image
import com.mhp.coding.challenges.mapping.models.db.blocks.*
import com.mhp.coding.challenges.mapping.models.dto.ArticleDto
import com.mhp.coding.challenges.mapping.models.dto.ImageDto
import com.mhp.coding.challenges.mapping.models.dto.blocks.*
import org.mapstruct.AfterMapping
import org.mapstruct.Mapper
import org.mapstruct.MappingTarget
import org.mapstruct.ReportingPolicy
import java.util.*

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
abstract class ArticleMapper {
    abstract fun map(article: Article?): ArticleDto

    @AfterMapping
    fun afterMap(article: Article, @MappingTarget articleDto: ArticleDto): ArticleDto {
        articleDto.blocks = articleDto.blocks.sortedBy { it.sortIndex }
        return articleDto
    }

    fun map(articleBlock: ArticleBlock): ArticleBlockDto {
        if (articleBlock is GalleryBlock) {
            return mapImpl(articleBlock);
        }
        if (articleBlock is ImageBlock) {
            return mapImpl(articleBlock);
        }
        if (articleBlock is TextBlock) {
            return mapImpl(articleBlock);
        }
        if (articleBlock is VideoBlock) {
            return mapImpl(articleBlock);
        }
        throw MappingException(articleBlock.javaClass.simpleName);
    }

    abstract fun map(image: Image): ImageDto

    abstract fun mapImpl(galleryBlock: GalleryBlock?): GalleryBlockDto
    abstract fun mapImpl(imageBlock: ImageBlock?): ImageBlockDto
    abstract fun mapImpl(textBlock: TextBlock?): TextBlockDto
    abstract fun mapImpl(videoBlock: VideoBlock): VideoBlockDto

    // Not part of the challenge / Nicht Teil dieser Challenge.
    fun map(articleDto: ArticleDto?): Article = Article(
        title = "An Article",
        blocks = emptySet(),
        id = 1,
        lastModified = Date()
    )
}
