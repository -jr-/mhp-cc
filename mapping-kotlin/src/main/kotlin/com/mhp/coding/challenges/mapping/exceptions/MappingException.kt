package com.mhp.coding.challenges.mapping.exceptions

class MappingException(notMappedClass: String?) : Exception("Mapping für '$notMappedClass' muss implementiert werden.") {
}