package com.mhp.coding.challenges.mapping.controllers

import com.mhp.coding.challenges.mapping.exceptions.ArticleNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class RestControllerExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [(ArticleNotFoundException::class)])
    fun handleArticleNotFoundException(ex: ArticleNotFoundException,request: WebRequest): ResponseEntity<String> {
        return ResponseEntity(ex.details, HttpStatus.NOT_FOUND)
    }
}