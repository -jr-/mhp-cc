package com.mhp.coding.challenges.mapping.exceptions

import java.lang.RuntimeException

class ArticleNotFoundException(id: Long) : RuntimeException() {
    val details: String

    init {
        details = "Artikel mit der Id $id wurde nicht gefunden."
    }
}